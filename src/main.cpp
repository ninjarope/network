//
//  ns_classes.cpp
//  ns_sketch
//
//  Created by Tommi Gröhn on 13.11.2015.
//  Copyright (c) 2015 tommigrohn. All rights reserved.
//

#include <iostream>
#include "NetworkSimulator.h"
#include "TestRouter.h"
#include "TestLink.h"
#include "PacketGenerator.h"
#include "PacketReceiver.h"

int main() {
    /* Simple test case. */
    
    // create some nodes
    std::vector<ApplicationNode*> nodes;
    nodes.push_back(new ApplicationNode("A"));
    nodes.push_back(new ApplicationNode("B"));
    nodes.push_back(new ApplicationNode("C"));
    nodes.push_back(new ApplicationNode("D"));
    nodes.push_back(new ApplicationNode("E"));
    
    // add applications (node-internal order of applications matters!)
    nodes[0]->addApplications({new PacketReceiver, new TestRouter});
    nodes[1]->addApplications({new PacketReceiver, new TestRouter});
    nodes[2]->addApplications({new PacketReceiver, new TestRouter});
    nodes[3]->addApplications({new PacketReceiver, new TestRouter});
    nodes[4]->addApplications({new PacketReceiver, new TestRouter});
   
    // create some links between nodes
    std::vector<Link*> links;
    links.push_back(new TestLink(nodes[0], nodes[1]));
    links.push_back(new TestLink(nodes[1], nodes[2]));
    links.push_back(new TestLink(nodes[2], nodes[3]));
    links.push_back(new TestLink(nodes[3], nodes[4]));
    links.push_back(new TestLink(nodes[4], nodes[0]));
    
    // add contents to simulator
    NetworkSimulator ns;
    ns.addNodes(nodes);
    ns.addLinks(links);
    
    // generate some traffic
    nodes[4]->addApplications(new PacketGenerator(2, ns.getNetworkState().getAddresses()));

    // run (timer has currently some hard-coded test values)
    ns.startTimer();
}

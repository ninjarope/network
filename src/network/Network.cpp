//
//  Network.cpp
//  ns_sketch
//
//  Created by Tommi Gröhn on 13.11.2015.
//  Copyright (c) 2015 tommigrohn. All rights reserved.
//

#include "Network.h"

Network::Network() {}

const std::vector<ApplicationNode*>& Network::getNodes() const { return nodes; }

const std::vector<Link*>& Network::getLinks() const { return links; }

size_t Network::getNodeCount() {
    return sizeof(nodes);
}

size_t Network::getLinkCount() {
    return sizeof(nodes);
}

std::vector<nsTypes::AddressType> Network::getAddresses() const {
    std::vector<nsTypes::AddressType> addresses;
    for (auto& n : nodes) {
        addresses.push_back(n->getAddress());
    }
    return addresses;
}

void Network::addNode(ApplicationNode* n) {
    if (n) nodes.push_back(n);
}

void Network::addLink(Link* l) {
    /* Check for invalid links */
    if (l && l->getSource() && l->getDestination())
        links.push_back(l);
}
